# RChain-CICD

## Objective

The objective of this challenge is to build a CI/CD pipeline to support Rchain development, dockerize Rchain, and test the docker image by deploying a 100 node subnet.

The following are the steps/resources we utilized for our configuration:

1. Set up a Gitlab CI pipeline that checks for the following
   - Check Rusts syntax / compile [[a]](https://medium.com/astraol/optimizing-ci-cd-pipeline-for-rust-projects-gitlab-docker-98df64ae3bc4)
   - Builds a new docker image [[b]](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
2. Configure ArgoCD to deploy the docker image to an EKS cluster. [[c]](https://levelup.gitconnected.com/gitops-in-kubernetes-with-gitlab-ci-and-argocd-9e20b5d3b55b)
3. After ArgoCD deploys the container, scale up the EKS cluster to replicate a 100 node testnet. [[d]](https://www.jeffgeerling.com/blog/2020/10000-kubernetes-pods-10000-subscribers)

## Architecture Design

![Image of Architecture Design](Rchain-CI_CD.png)

## Contributors

|      [<img src="https://gitlab.com/uploads/-/system/user/avatar/5359745/avatar.png?width=400" width="100px;"/><br /><sub><b>Khristian Brooks</b></sub>](https://gitlab.com/_kcode)<br />      | [<img src="https://secure.gravatar.com/avatar/89dc330611c404769bc27f28be7c810c?s=800&d=identicon" width="100px;"/><br /><sub><b>Kaavya Krishna-Kumar</b></sub>](https://gitlab.com/kaavya1698)<br /> |
| :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [<img src="https://secure.gravatar.com/avatar/21a8f066c7e9f2c3e5363299d397b830?s=800&d=identicon" width="100px;"/><br /><sub><b>Shaliya Sultana</b></sub>](https://gitlab.com/Shaliyaj)<br /> |      [<img src="https://secure.gravatar.com/avatar/a2645c92bfe98732a7a8f7b23b0e371e?s=800&d=identicon" width="100px;"/><br /><sub><b>Zachari Ramos</b></sub>](https://gitlab.com/zramos2)<br />      |
