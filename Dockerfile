# ------------------------------------------------------------------------------
# Cargo Build Stage
# ------------------------------------------------------------------------------

FROM rust:latest as cargo-build

RUN apt-get update

RUN apt-get install musl-tools -y

RUN rustup target add x86_64-unknown-linux-musl

WORKDIR /usr/src/rust-ci-cd-challenge

COPY Cargo.toml Cargo.toml

RUN mkdir src/

RUN echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs

RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl

RUN rm -f target/x86_64-unknown-linux-musl/release/deps/rust-ci-cd-challenge*

COPY . .

RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------

FROM alpine:latest

RUN addgroup -g 1000 rust-ci-cd-challenge

RUN adduser -D -s /bin/sh -u 1000 -G rust-ci-cd-challenge rust-ci-cd-challenge

WORKDIR /home/rust-ci-cd-challenge/bin/

COPY --from=cargo-build /usr/src/rust-ci-cd-challenge/target/x86_64-unknown-linux-musl/release/rust-ci-cd-challenge .

RUN chown rust-ci-cd-challenge:rust-ci-cd-challenge rust-ci-cd-challenge

USER rust-ci-cd-challenge

CMD ["./rust-ci-cd-challenge"]